package academy.devdojo.maratonajava.introducao;

public class Aula07Arrays03 {
    public static void main(String[] args) {
        String[] nomes = new String[4];
        nomes[0] = "Maria";
        nomes[1] = "João";
        nomes[2] = "José";

        for (int i = 0; i < nomes.length; i++) {
            System.out.println("Primeiro nome do Array de indice " + i + " -> " + nomes[i]);
        }
    }
}
