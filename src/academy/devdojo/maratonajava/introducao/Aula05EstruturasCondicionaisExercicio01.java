package academy.devdojo.maratonajava.introducao;

public class Aula05EstruturasCondicionaisExercicio01 {
    public static void main(String[] args) {
        System.out.println("Estruturas condicionais Exercício 01:");
        double salarioAnual = 68508;
        double primeiraFaixa = 9.70 / 100;
        double segundaFaixa = 37.35 / 100;
        double terceiraFaixa = 49.50 / 100;
        double resultado;

        if (salarioAnual <= 34712) {
            resultado = salarioAnual * primeiraFaixa;
        } else if (salarioAnual > 34712 && salarioAnual <= 68507) {
            resultado = salarioAnual * segundaFaixa;
        } else {
            resultado = salarioAnual * terceiraFaixa;
        }

        System.out.println("\nSeu salário anual de " + salarioAnual + " terá uma taxa de: " + resultado);
    }
}
