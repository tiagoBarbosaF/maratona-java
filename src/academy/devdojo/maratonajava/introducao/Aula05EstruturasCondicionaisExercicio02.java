package academy.devdojo.maratonajava.introducao;

public class Aula05EstruturasCondicionaisExercicio02 {
    public static void main(String[] args) {
        System.out.println("Estruturas condicionais Exercício 02:");
        // Utilizando Switch e dados os valores de 1 a 7, imprima se é dia útil ou final de semana
        // Considerando 1 como domingo

        byte dia = 2;
        switch (dia) {
            case 1:
            case 7:
                System.out.println("Final de Semana");
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                System.out.println("Dia útil");
                break;
            default:
                System.out.println("Valor Inválido");
                break;
//        switch (dia) {
//            case 1:
//                System.out.println("\nDia da semana é Domingo e é final de semana");
//                break;
//            case 2:
//                System.out.println("\nDia da semana é Segunda e é dia útil");
//                break;
//            case 3:
//                System.out.println("\nDia da semana é Terça e é dia útil");
//                break;
//            case 4:
//                System.out.println("\nDia da semana é Quarta e é dia útil");
//                break;
//            case 5:
//                System.out.println("\nDia da semana é Quinta e é dia útil");
//                break;
//            case 6:
//                System.out.println("\nDia da semana é Sexta e é dia útil");
//                break;
//            case 7:
//                System.out.println("\nDia da semana é Sábado e é final de semana");
//                break;
//            default:
//                System.out.println("\nValor inválido");
//                break;
        }
    }
}
