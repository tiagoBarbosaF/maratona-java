package academy.devdojo.maratonajava.introducao;

public class Aula04Operadores {
    public static void main(String[] args) {
//        Operadores básicos -> + , - , / , *
        System.out.println("Operadores básicos:");
        int numero01 = 10;
        int numero02 = 20;
        double resultadoSoma = numero01 + numero02;
        double resultadoSubtracao = numero01 - numero02;
        double resultadoMultiplicacao = numero01 * numero02;
        double resultadoDivisao = numero01 / numero02;

        System.out.println("Soma -> " + resultadoSoma);
        System.out.println("Subtração -> " + resultadoSubtracao);
        System.out.println("Multiplicação -> " + resultadoMultiplicacao);
        System.out.println("Divisão -> " + resultadoDivisao);

//        Operador resto -> %
        System.out.println("\nOperador resto:");
        int resto = 20 % 7;

        System.out.println("Resto -> " + resto);

//        Operadores comparativos ->  < , > , <= , >= , == , !=
        System.out.println("\nOperadores comparativos:");
        boolean idDezMaiorQueVinte = 10 > 20;
        boolean idDezMenorQueVinte = 10 < 20;
        boolean idDezIgualVinte = 10 == 20;
        boolean idDezIgualDez = 10 == 10;
        boolean idDezDiferenteDez = 10 != 10;

        System.out.println("idDezMaiorQueVinte -> " + idDezMaiorQueVinte);
        System.out.println("idDezMenorQueVinte -> " + idDezMenorQueVinte);
        System.out.println("idDezIgualVinte -> " + idDezIgualVinte);
        System.out.println("idDezIgualDez -> " + idDezIgualDez);
        System.out.println("idDezDiferenteDez -> " + idDezDiferenteDez);

//        Operadores logicos -> && (AND), || (OR)
        System.out.println("\nOperadores logicos && (AND):");
        int age = 29;
        float salary = 3500F;
        boolean isDentroDaLeiMaiorQueTrinta = age >= 30 && salary >= 4612;
        boolean isDentroDaLeiMenorQueTrinta = age < 30 && salary >= 3381;

        System.out.println("isDentroDaLeiMaiorQueTrinta " + isDentroDaLeiMaiorQueTrinta);
        System.out.println("isDentroDaLeiMenorQueTrinta " + isDentroDaLeiMenorQueTrinta);

        System.out.println("\nOperadores logicos || (OR):");
        double valorTotalContaCorrente = 200;
        double valorTotalContaPoupanca = 10000;
        float valorPlaystation = 5000F;
        boolean isPlaystationCincoCompravel = valorTotalContaCorrente > valorPlaystation || valorTotalContaPoupanca > valorPlaystation;

        System.out.println("isPlaystationCincoCompravel " + isPlaystationCincoCompravel);

//        Operadores de atribuicao ->  = , += , -= , *= , /= , %=
        System.out.println("\nOperadores de atribuição ->  = , += , -= , *= , /= , %=:");

        double bonus = 1800;
        bonus += 1000; // igual a bonus = bonus + 1000
        bonus -= 1000; // igual a bonus = bonus - 1000
        bonus *= 2; // igual a bonus = bonus * 2
        bonus /= 2; // igual a bonus = bonus / 2
        bonus %= 2; // igual a bonus = bonus % 2

        System.out.println(bonus);

//        Operadores de atribuicao -> ++ , --
        System.out.println("\nOperadores de atribuição -> ++ , --:");
        int contador = 0;
        contador += 1;
        contador++;
        contador--;
        ++contador;
        --contador;

        int contador2 = 0;
        System.out.println("Incremento após -> " + contador2++);
        System.out.println("Incremento antes -> " + ++contador2);
    }
}
