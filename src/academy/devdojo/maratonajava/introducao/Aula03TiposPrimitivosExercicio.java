package academy.devdojo.maratonajava.introducao;
/*
* Prática
*
* Crie vari[aveis para os campos descritos abaixo entre <> e imprima
* a seguinte mensagem:
*
* Eu <nome>, morando no endereço <endereco>,
* confirmo que recebi o salário de <salario>, na data <data>
* */
public class Aula03TiposPrimitivosExercicio {
    public static void main(String[] args) {
        String nome = "Tiago Barbosa";
        String endereco = "Rua da Paz - 1273";
        double salario = 4500.76;
        String dataRecebimentoSalario = "01.05.2021";
        String relatorio = "Eu "+nome+", morando no endereço "+endereco+
                           ",\nconfirmo que recebi o salário de "+salario+", na data "+dataRecebimentoSalario;

        System.out.println(relatorio); //possível de reaproveitamento
        System.out.println("Eu " + nome + ", morando no endereço " + endereco +
                           ",\nconfirmo que recebi o salário de " + salario + ", na data " + dataRecebimentoSalario); //uso somente nesta parte
    }
}
