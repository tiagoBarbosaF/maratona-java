package academy.devdojo.maratonajava.introducao;

public class Aula02TiposPrimitivos {
    public static void main(String[] args) {
//        Tipos primitivos -> int, double, float, char, byte, short, long, boolean
//        Cast é o jeito de "forçar" um valor que não corresponde ao tipo especificado.
//        Exemplo: int ageCast = (int) 10000000000L;
        int age = 10;
//        int ageCast = (int) 10000000000L;
        long bigNumber = 100000;
//        long bigNumberCast = (long) 155.23;
        double salaryDouble = 2000.0D;
        float salaryFloat = (float) 2500.0D;
        byte ageByte = 10;
        short ageShort = 10;
        boolean verdadeiro = true;
        boolean falso = false;
        char caractereASC = 65;
        char caractereUnicode = '\u0041';
        String text = "Isso é um texto graaaaaaaaaande.";

        System.out.println("A idade é " + age + " anos.");
        System.out.println("Int -> " + age);
        System.out.println("Long -> " + bigNumber);
        System.out.println("Double -> " + salaryDouble);
        System.out.println("Float -> " + salaryFloat);
        System.out.println("Byte -> " + ageByte);
        System.out.println("Short -> " + ageShort);
        System.out.println("Boolean True -> " + verdadeiro);
        System.out.println("Boolean False -> " + falso);
        System.out.println("Caractere ASC -> " + caractereASC);
        System.out.println("Caractere Unicode -> " + caractereUnicode);
        System.out.println(text);
    }
}
