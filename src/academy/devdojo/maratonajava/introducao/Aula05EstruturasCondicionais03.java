package academy.devdojo.maratonajava.introducao;

public class Aula05EstruturasCondicionais03 {
    public static void main(String[] args) {
        System.out.println("Operador ternário:");
        // Modelo -> (condicao) ? verdadeiro : falso
        // Doar se salario > 5000
        double salario = 6000;
//        String mensagemDoar = "Eu vou doar 500 para o DevDojo";
//        String mensagemNaoDoar = "Ainda não tenho condições, mais vou ter!";
//        String resultado = salario > 5000 ? mensagemDoar : mensagemNaoDoar;
        String resultado = salario > 5000 ? "Eu vou doar 500 para o DevDojo" : "Ainda não tenho condições, mais vou ter!";
        System.out.println(resultado);

    }
}
