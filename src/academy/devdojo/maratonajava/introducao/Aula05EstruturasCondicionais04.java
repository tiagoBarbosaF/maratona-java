package academy.devdojo.maratonajava.introducao;

public class Aula05EstruturasCondicionais04 {
    public static void main(String[] args) {
        System.out.println("Switch:");
        // Imprima o dia da semana, considerando 1 como domingo

        byte dia = 3;

        switch (dia) { // tipos que o switch aceita: char, int, byte, short, enum, String
            case 1:
                System.out.println("\nDia da semana -> Domingo");
                break;
            case 2:
                System.out.println("\nDia da semana -> Segunda");
                break;
            case 3:
                System.out.println("\nDia da semana -> Terça");
                break;
            case 4:
                System.out.println("\nDia da semana -> Quarta");
                break;
            case 5:
                System.out.println("\nDia da semana -> Quinta");
                break;
            case 6:
                System.out.println("\nDia da semana -> Sexta");
                break;
            case 7:
                System.out.println("\nDia da semana -> Sábado");
                break;
            default:
                System.out.println("\nOpção inválida.");
                break;
        }

        char sexo = 'M';
        switch (sexo){
            case 'M':
                System.out.println("Sexo -> Masculino");
                break;
            case 'F':
                System.out.println("Sexo -> Feminino");
                break;
            default:
                System.out.println("Inválido");
                break;
        }
    }
}
