package academy.devdojo.maratonajava.introducao;

public class Aula07Arrays02 {
    public static void main(String[] args) {
        // byte, short, int, long, float e double imprime -> 0
        // char '\u0000' imprime -> ''
        // boolean imprime -> false
        // String imprime -> null

        System.out.println("Retorno do tipo byte não declarado:");
        byte[] idades01 = new byte[3];
        System.out.println(idades01[0]);
        System.out.println(idades01[1]);
        System.out.println(idades01[2]);

        System.out.println("\nRetorno do tipo short não declarado:");
        short[] idades02 = new short[3];
        System.out.println(idades02[0]);
        System.out.println(idades02[1]);
        System.out.println(idades02[2]);

        System.out.println("\nRetorno do tipo int não declarado:");
        int[] idades03 = new int[3];
        System.out.println(idades03[0]);
        System.out.println(idades03[1]);
        System.out.println(idades03[2]);

        System.out.println("\nRetorno do tipo long não declarado:");
        long[] idades04 = new long[3];
        System.out.println(idades04[0]);
        System.out.println(idades04[1]);
        System.out.println(idades04[2]);

        System.out.println("\nRetorno do tipo float não declarado:");
        float[] idades05 = new float[3];
        System.out.println(idades05[0]);
        System.out.println(idades05[1]);
        System.out.println(idades05[2]);

        System.out.println("\nRetorno do tipo double não declarado:");
        double[] idades06 = new double[3];
        System.out.println(idades06[0]);
        System.out.println(idades06[1]);
        System.out.println(idades06[2]);

        System.out.println("\nRetorno do tipo char não declarado:");
        char[] nome01 = new char[3];
        System.out.println(nome01[0]);
        System.out.println(nome01[1]);
        System.out.println(nome01[2]);

        System.out.println("\nRetorno do tipo boolean não declarado:");
        boolean[] nome02 = new boolean[3];
        System.out.println(nome02[0]);
        System.out.println(nome02[1]);
        System.out.println(nome02[2]);

        System.out.println("\nRetorno do tipo String não declarado:");
        String[] nome03 = new String[3];
        System.out.println(nome03[0]);
        System.out.println(nome03[1]);
        System.out.println(nome03[2]);
    }
}
