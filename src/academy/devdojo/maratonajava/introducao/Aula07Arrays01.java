package academy.devdojo.maratonajava.introducao;

public class Aula07Arrays01 {
    public static void main(String[] args) {
        int[] idades = new int[3]; // Arrays são do tipo reference e não primitivos
        idades[0] = 21;
        idades[1] = 15;
        idades[2] = 11;

        System.out.println("Array de posição 0 -> " + idades[0]);
        System.out.println("Array de posição 1 -> " + idades[1]);
        System.out.println("Array de posição 2 -> " + idades[2]);
    }
}
