package academy.devdojo.maratonajava.introducao;

public class Aula06EstruturasDeRepeticao01 {
    public static void main(String[] args) {
        System.out.println("Estruturas de repetição:");
        // while, do while, for

        int count = 0;
        while (count < 10) {
            System.out.println("While -> " + ++count);
//            count = count + 1;
//            count += 1;
//            count++;
        }
        System.out.println();
        count = 0;
        do {
            System.out.println("Dentro do do-while -> " + ++count);
        } while (count < 10);
        System.out.println();
        for (int i = 0; i < 10; i++) {
            System.out.println("For -> " + i);
        }
    }
}
