package academy.devdojo.maratonajava.javacore.Fmodificadorestatico.dominio;

public class Car {
    private String nome;
    private double velociadeMaxima;
    private static double velocidadeLimite = 250;

    public Car(String nome, double velociadeMaxima) {
        this.nome = nome;
        this.velociadeMaxima = velociadeMaxima;
    }

    public void imprime() {
        System.out.println("Nome: " + this.nome + " Velocidade Máxima: " + this.velociadeMaxima +
                         " Velocidade Limite: " + Car.velocidadeLimite);
    }

    public static void setVelocidadeLimite(double velocidadeLimite){
        Car.velocidadeLimite = velocidadeLimite;
    }

    public static double getVelocidadeLimite(){
        return Car.velocidadeLimite;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getVelociadeMaxima() {
        return velociadeMaxima;
    }

    public void setVelociadeMaxima(double velociadeMaxima) {
        this.velociadeMaxima = velociadeMaxima;
    }
}
