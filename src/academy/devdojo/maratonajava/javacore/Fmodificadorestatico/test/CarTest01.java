package academy.devdojo.maratonajava.javacore.Fmodificadorestatico.test;

import academy.devdojo.maratonajava.javacore.Fmodificadorestatico.dominio.Car;

public class CarTest01 {
    public static void main(String[] args) {
        Car car01 = new Car("Ferrari",350);
        Car car02 = new Car("Golf",280);
        Car car03 = new Car("C250",300);

//        Car.velocidadeLimite = 180;

        car01.imprime();
        car02.imprime();
        car03.imprime();
    }
}
