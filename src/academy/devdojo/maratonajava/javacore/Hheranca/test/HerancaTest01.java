package academy.devdojo.maratonajava.javacore.Hheranca.test;

import academy.devdojo.maratonajava.javacore.Hheranca.dominio.Endereco;
import academy.devdojo.maratonajava.javacore.Hheranca.dominio.Funcionario;
import academy.devdojo.maratonajava.javacore.Hheranca.dominio.Pessoa;

public class HerancaTest01 {
    public static void main(String[] args) {
        Endereco endereco = new Endereco();
        endereco.setRua("Rua 3");
        endereco.setCep("12345-678");

        Pessoa pessoa = new Pessoa("José Silva");
        pessoa.setCpf("111.222.333-44");
        pessoa.setEndereco(endereco);

        pessoa.imprime();

        Funcionario funcionario = new Funcionario("Ana Maria");
        funcionario.setCpf("123.456.789-10");
        funcionario.setEndereco(endereco);
        funcionario.setSalario(5000);
        System.out.println("----------------");
        funcionario.imprime();
    }
}
