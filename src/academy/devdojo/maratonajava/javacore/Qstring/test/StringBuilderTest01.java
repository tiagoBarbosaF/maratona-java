package academy.devdojo.maratonajava.javacore.Qstring.test;

public class StringBuilderTest01 {
    public static void main(String[] args) {
        String nome = "Tiago Barbosa";
        nome.concat(" DevDojo");
        nome.substring(0,3);
        System.out.println(nome);
        StringBuilder sb = new StringBuilder("Tiago Barbosa");
        sb.append(" DevDojo").append(" Academy");
        sb.reverse();
        sb.delete(0,3);
        System.out.println(sb);
    }
}
