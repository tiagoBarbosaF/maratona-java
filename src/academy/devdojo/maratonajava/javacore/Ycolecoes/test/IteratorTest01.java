package academy.devdojo.maratonajava.javacore.Ycolecoes.test;

import academy.devdojo.maratonajava.javacore.Ycolecoes.dominio.Manga;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class IteratorTest01 {
    public static void main(String[] args) {
        List<Manga> mangas = new LinkedList<>();
        mangas.add(new Manga(5L,"Hellsing Ultimate",19.99,0));
        mangas.add(new Manga(1L,"Berserk",6.50, 5));
        mangas.add(new Manga(4L,"Pokemon",12.80, 0));
        mangas.add(new Manga(3L,"Attack on Titan",20.30, 0));
        mangas.add(new Manga(2L,"Dragon Ball Z",21.40, 1));

//        Iterator<Manga> mangaIterator = mangas.iterator();
//        while (mangaIterator.hasNext()){
//            if (mangaIterator.next().getQuantidade() == 0){
//                mangaIterator.remove();
//            }
//        }

        mangas.removeIf(manga -> manga.getQuantidade() == 0);
        System.out.println(mangas);
    }
}
