package academy.devdojo.maratonajava.javacore.Ycolecoes.test;


import academy.devdojo.maratonajava.javacore.Ycolecoes.dominio.Consumidor;
import academy.devdojo.maratonajava.javacore.Ycolecoes.dominio.Manga;

import java.util.HashMap;
import java.util.Map;

public class MapTest02 {
    public static void main(String[] args) {
        Consumidor consumidor1 = new Consumidor("Tiago Barbosa");
        Consumidor consumidor2 = new Consumidor("DevDojo");

        Manga manga1 = new Manga(5L, "Hellsing Ultimate", 19.99);
        Manga manga2 = new Manga(1L,"Berserk",6.50);
        Manga manga3 = new Manga(4L,"Pokemon",12.80);
        Manga manga4 = new Manga(3L,"Attack on Titan",20.30);
        Manga manga5 = new Manga(2L, "Dragon Ball Z", 21.40);

        Map<Consumidor, Manga> consumidorManga = new HashMap<>();
        consumidorManga.put(consumidor1, manga3);
        consumidorManga.put(consumidor2, manga5);

        for (Map.Entry<Consumidor, Manga> entry : consumidorManga.entrySet()){
            System.out.println(entry.getKey().getNome()+" comprou o mangá -> "+entry.getValue().getNome());
        }
    }
}
