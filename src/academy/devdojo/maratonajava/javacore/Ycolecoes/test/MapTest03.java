package academy.devdojo.maratonajava.javacore.Ycolecoes.test;


import academy.devdojo.maratonajava.javacore.Ycolecoes.dominio.Consumidor;
import academy.devdojo.maratonajava.javacore.Ycolecoes.dominio.Manga;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapTest03 {
    public static void main(String[] args) {
        Consumidor consumidor1 = new Consumidor("Tiago Barbosa");
        Consumidor consumidor2 = new Consumidor("DevDojo");

        Manga manga1 = new Manga(5L, "Hellsing Ultimate", 19.99);
        Manga manga2 = new Manga(1L,"Berserk",6.50);
        Manga manga3 = new Manga(4L,"Pokemon",12.80);
        Manga manga4 = new Manga(3L,"Attack on Titan",20.30);
        Manga manga5 = new Manga(2L, "Dragon Ball Z", 21.40);

        List<Manga> mangaConsumidor1List = List.of(manga1,manga2, manga3);
        List<Manga> mangaConsumidor2List = List.of(manga4,manga2);
        Map<Consumidor, List<Manga>> consumidorMangaMap = new HashMap<>();
        consumidorMangaMap.put(consumidor1, mangaConsumidor1List);
        consumidorMangaMap.put(consumidor2, mangaConsumidor2List);

        for (Map.Entry<Consumidor, List<Manga>> entry : consumidorMangaMap.entrySet()) {
            System.out.println("Cliente --> " + entry.getKey().getNome());
            for (Manga manga : entry.getValue()) {
                System.out.println("Comprou ------> "+manga.getNome());
            }

        }

    }
}
