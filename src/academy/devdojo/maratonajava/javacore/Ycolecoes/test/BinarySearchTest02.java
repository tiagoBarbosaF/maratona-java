package academy.devdojo.maratonajava.javacore.Ycolecoes.test;

import academy.devdojo.maratonajava.javacore.Ycolecoes.dominio.Manga;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BinarySearchTest02 {
    public static void main(String[] args) {
        MangaByIdComparator mangaByIdComparator = new MangaByIdComparator();
        List<Manga> mangas = new ArrayList<>(6);
        mangas.add(new Manga(5L,"Hellsing Ultimate",19.99));
        mangas.add(new Manga(1L,"Berserk",6.50));
        mangas.add(new Manga(4L,"Pokemon",12.80));
        mangas.add(new Manga(3L,"Attack on Titan",20.30));
        mangas.add(new Manga(2L,"Dragon Ball Z",21.40));

//        Collections.sort(mangas);
        mangas.sort(mangaByIdComparator);
        for (Manga manga: mangas){
            System.out.println(manga);
        }

        Manga mangaToSearch = new Manga(2L,"Dragon Ball Z",21.40);

        System.out.println(Collections.binarySearch(mangas,mangaToSearch,mangaByIdComparator));
    }
}
