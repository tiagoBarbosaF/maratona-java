package academy.devdojo.maratonajava.javacore.Ycolecoes.test;

import academy.devdojo.maratonajava.javacore.Ycolecoes.dominio.Manga;
import academy.devdojo.maratonajava.javacore.Ycolecoes.dominio.Smartphone;

import java.util.Comparator;
import java.util.NavigableSet;
import java.util.TreeSet;

class SmartphoneMarcaComparator implements Comparator<Smartphone>{

    @Override
    public int compare(Smartphone o1, Smartphone o2) {
        return o1.getMarca().compareTo(o2.getMarca());
    }
}

class MangaPrecoComparator implements Comparator<Manga>{

    @Override
    public int compare(Manga o1, Manga o2) {
        return Double.compare(o1.getPreco(), o2.getPreco());
    }
}

public class NavigableSetTest01 {
    public static void main(String[] args) {
        NavigableSet<Smartphone> set = new TreeSet<>(new SmartphoneMarcaComparator());
        Smartphone smartphone = new Smartphone("123", "Nokia");
        set.add(smartphone);
        System.out.println(smartphone);
        System.out.println("-----------------------");

        NavigableSet<Manga> mangas = new TreeSet<>(new MangaPrecoComparator());
        mangas.add(new Manga(5L,"Hellsing Ultimate",19.99,0));
        mangas.add(new Manga(1L,"Berserk",6.50, 5));
        mangas.add(new Manga(4L,"Pokemon",12.80, 0));
        mangas.add(new Manga(3L,"Attack on Titan",20.30, 0));
        mangas.add(new Manga(2L,"Dragon Ball Z",21.40, 1));

        for (Manga manga : mangas){
            System.out.println(manga);
        }

        Manga yuyu = new Manga(2L, "YuYu Hakusho", 8, 1);

        // lower <
        // floor <=
        // higher >
        // ceiling >=
        System.out.println("------------------");
        System.out.println(mangas.lower(yuyu));
        System.out.println(mangas.floor(yuyu));
        System.out.println(mangas.higher(yuyu));
        System.out.println(mangas.ceiling(yuyu));

        // Metodos de remoção
        System.out.println(mangas.size());
        System.out.println(mangas.pollFirst()); // trás o primeiro
        System.out.println(mangas.size());

    }
}
