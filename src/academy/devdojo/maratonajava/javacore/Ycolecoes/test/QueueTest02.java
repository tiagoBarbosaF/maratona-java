package academy.devdojo.maratonajava.javacore.Ycolecoes.test;

import academy.devdojo.maratonajava.javacore.Ycolecoes.dominio.Manga;

import java.util.PriorityQueue;
import java.util.Queue;

public class QueueTest02 {
    public static void main(String[] args) {
        Queue<Manga> mangas = new PriorityQueue<>(new MangaPrecoComparator().reversed());
        mangas.add(new Manga(5L,"Hellsing Ultimate",19.99,0));
        mangas.add(new Manga(1L,"Berserk",6.50, 5));
        mangas.add(new Manga(4L,"Pokemon",12.80, 0));
        mangas.add(new Manga(3L,"Attack on Titan",20.30, 0));
        mangas.add(new Manga(2L,"Dragon Ball Z",21.40, 1));

        while (!mangas.isEmpty()){
            System.out.println(mangas.poll());
        }
    }
}
