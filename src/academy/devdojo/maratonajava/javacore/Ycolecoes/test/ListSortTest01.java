package academy.devdojo.maratonajava.javacore.Ycolecoes.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListSortTest01 {
    public static void main(String[] args) {
        List<String> mangas = new ArrayList<>(6);
        mangas.add("Hellsing Ultimate");
        mangas.add("Berserk");
        mangas.add("Pokemon");
        mangas.add("Attack on Titan");
        mangas.add("Dragon Ball Z");

        System.out.println("Mangás não ordenados: " + mangas);
        Collections.sort(mangas);
        System.out.println("Mangás ordenados: " + mangas);

        List<Double> dinheiros = new ArrayList<>();
        dinheiros.add(100.21);
        dinheiros.add(23.98);
        dinheiros.add(21.19);
        dinheiros.add(98.10);

        for (String manga : mangas) {
            System.out.print(manga + ", ");
        }


        System.out.println("\n" + dinheiros);
        Collections.sort(dinheiros);
        System.out.println(dinheiros);
    }
}
