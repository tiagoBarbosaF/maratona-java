package academy.devdojo.maratonajava.javacore.Ycolecoes.test;

import academy.devdojo.maratonajava.javacore.Ycolecoes.dominio.Manga;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class SetTest01 {
    public static void main(String[] args) {
        Set<Manga> mangas = new LinkedHashSet<>();
        mangas.add(new Manga(5L,"Hellsing Ultimate",19.99,0));
        mangas.add(new Manga(1L,"Berserk",6.50, 5));
        mangas.add(new Manga(4L,"Pokemon",12.80, 0));
        mangas.add(new Manga(3L,"Attack on Titan",20.30, 0));
        mangas.add(new Manga(2L,"Dragon Ball Z",21.40, 1));
        mangas.add(new Manga(2L,"Dragon Ball Z",21.40, 1)); // duplicando

        for (Manga manga : mangas){
            System.out.println(manga);
        }
    }
}
