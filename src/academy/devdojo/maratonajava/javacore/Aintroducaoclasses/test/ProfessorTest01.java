package academy.devdojo.maratonajava.javacore.Aintroducaoclasses.test;

import academy.devdojo.maratonajava.javacore.Aintroducaoclasses.domain.Professor;

public class ProfessorTest01 {
    public static void main(String[] args) {
        Professor professor = new Professor();

        professor.name = "João";
        professor.age = 40;
        professor.sex = 'M';

        System.out.printf("Professor -> Nome: " + professor.name + " Age: " + professor.age + " Sex: " + professor.sex);
    }
}
