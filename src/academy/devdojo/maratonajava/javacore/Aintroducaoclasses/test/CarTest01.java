package academy.devdojo.maratonajava.javacore.Aintroducaoclasses.test;

import academy.devdojo.maratonajava.javacore.Aintroducaoclasses.domain.Car;

public class CarTest01 {
    public static void main(String[] args) {
        Car car1 = new Car();
        Car car2 = new Car();

        car1.name = "Toro";
        car1.model = "Volcano";
        car1.year = 2021;

        car2.name = "Toro";
        car2.model = "Freedom";
        car2.year = 2021;

        System.out.println("Car -> Name: " + car1.name + " | Model: " + car1.model + " | Year: " + car1.year);
        System.out.println("Car -> Name: " + car2.name + " | Model: " + car2.model + " | Year: " + car2.year);
    }
}
