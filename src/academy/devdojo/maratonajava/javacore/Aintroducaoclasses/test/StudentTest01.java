package academy.devdojo.maratonajava.javacore.Aintroducaoclasses.test;

import academy.devdojo.maratonajava.javacore.Aintroducaoclasses.domain.Student;

public class StudentTest01 {
    public static void main(String[] args) {
        Student student = new Student();

        student.name = "José";
        student.age = 21;
        student.sex = 'M';

        System.out.println("Nome: " + student.name);
        System.out.println("Age: " + student.age);
        System.out.println("Sex: " + student.sex);
    }
}
