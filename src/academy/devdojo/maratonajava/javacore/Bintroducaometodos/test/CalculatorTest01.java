package academy.devdojo.maratonajava.javacore.Bintroducaometodos.test;

import academy.devdojo.maratonajava.javacore.Bintroducaometodos.dominio.Calculator;

public class CalculatorTest01 {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        System.out.println("Result for Sum:");
        calculator.sumTwoNumbers();

        System.out.println("Result for Subtract:");
        calculator.subtractTwoNumbers();
    }
}
