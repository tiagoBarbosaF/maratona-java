package academy.devdojo.maratonajava.javacore.Bintroducaometodos.test;

import academy.devdojo.maratonajava.javacore.Bintroducaometodos.dominio.Student;

public class StudentTest02 {
    public static void main(String[] args) {
        Student student01 = new Student();
        Student student02 = new Student();

        student01.name = "Peter";
        student01.age = 18;
        student01.sex = 'M';

        student02.name = "Maria";
        student02.age = 17;
        student02.sex = 'F';

        student01.print();
        student02.print();

    }
}
