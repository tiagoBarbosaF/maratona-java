package academy.devdojo.maratonajava.javacore.Bintroducaometodos.test;

import academy.devdojo.maratonajava.javacore.Bintroducaometodos.dominio.PrintStudent;
import academy.devdojo.maratonajava.javacore.Bintroducaometodos.dominio.Student;

public class StudentTest01 {
    public static void main(String[] args) {
        Student student01 = new Student();
        Student student02 = new Student();
        PrintStudent printer = new PrintStudent();

        student01.name = "Peter";
        student01.age = 18;
        student01.sex = 'M';

        student02.name = "Maria";
        student02.age = 17;
        student02.sex = 'F';

        printer.print(student01);
        printer.print(student02);

//        System.out.println("Student 01 -> Name: " + student01.name + " | Age: " + student01.age + " | Sex: " + student01.sex);
//        System.out.println("Student 02 -> Name: " + student02.name + " | Age: " + student02.age + " | Sex: " + student02.sex);

    }
}
