package academy.devdojo.maratonajava.javacore.Bintroducaometodos.test;

import academy.devdojo.maratonajava.javacore.Bintroducaometodos.dominio.Employee;

public class EmplyeesTest01 {
    public static void main(String[] args) {
        Employee employee = new Employee();

        employee.setName("Maria");
        employee.setAge(32);
        employee.setSalarys(new double[]{1200, 987.32, 2000});

        employee.printEmployee();
    }
}
