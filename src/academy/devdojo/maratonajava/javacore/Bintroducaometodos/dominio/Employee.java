package academy.devdojo.maratonajava.javacore.Bintroducaometodos.dominio;

public class Employee {
    private String name;
    private int age;
    private double[] salarys;
    private double avg = 0;

    public void printEmployee() {
        System.out.println("Employee:");
        System.out.println("Name: " + this.name);
        System.out.println("Age: " + this.age);
        System.out.print("Salarys: ");
        if (salarys == null) {
            return;
        }
        for (double salary : salarys) {
            System.out.print(salary + " - ");
        }
        printSalarys();
    }

    public void printSalarys() {
        if (salarys == null) {
            return;
        }

        for (double salary : salarys) {
            avg += salary;
        }
        avg /= salarys.length;
        System.out.println("\nAverage Salarys: " + avg);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double[] getSalarys() {
        return salarys;
    }

    public void setSalarys(double[] salarys) {
        this.salarys = salarys;
    }

    public double getAvg() {
        return avg;
    }
}
