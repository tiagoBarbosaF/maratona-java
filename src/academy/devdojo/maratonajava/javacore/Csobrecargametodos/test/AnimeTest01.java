package academy.devdojo.maratonajava.javacore.Csobrecargametodos.test;

import academy.devdojo.maratonajava.javacore.Csobrecargametodos.dominio.Anime;

public class AnimeTest01 {
    public static void main(String[] args) {
        Anime anime = new Anime();
//        anime.init("Castlevania","TV", 12);
        anime.init("Castlevania","TV", 12,"Ação");
//        anime.setNome("Castlevania");
//        anime.setTipo("TV");
//        anime.setEpisodios(12);

        anime.imprime();
    }
}
