package academy.devdojo.maratonajava.javacore.Jmodificardorfinal.test;

import academy.devdojo.maratonajava.javacore.Jmodificardorfinal.dominio.Carro;
import academy.devdojo.maratonajava.javacore.Jmodificardorfinal.dominio.Ferrari;

public class CarroTest01 {
    public static void main(String[] args) {
        Carro carro = new Carro();

        System.out.println(Carro.VELOCIDADE_LIMITE);
        System.out.println(carro.COMPRADOR);
        carro.COMPRADOR.setNome("Maria");
        System.out.println(carro.COMPRADOR);

        Ferrari ferrari = new Ferrari();
        ferrari.setNome("Enzo");
        ferrari.imprime();
    }
}
