package academy.devdojo.maratonajava.javacore.Uregex.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcherTest03 {
    public static void main(String[] args) {
        // \d = Todos os digitos
        // \D = Tudo o que não for digito
        // \s = Espaços em branco --> No java os códigos que aplicam espaços em branco (\t, \n, \f, \r)
        // \S = Todos os caracteres excluindo os brancos
        // \w = a-z , A-Z, digitos, underline
        // \W = Tudo o que não for incluso no \w
        // [] = range
        String regex1 = "\\d";
        String regex2 = "\\D";
        String regex3 = "\\s";
        String regex4 = "\\S";
        String regex5 = "\\w";
        String regex6 = "\\W";
        String regex7 = "[a-zA-C]";
        String regex8 = "0[xX][0-9a-fA-F]";
        String texto = "@#@@ˆˆ%#$weoir u39rei3pr9 38r-91ieruq";
        String texto1 = "cafeBABE";
        String texto2 = "12 0x 0X 0xFFABC 0x109 0x1";
        Pattern pattern1 = Pattern.compile(regex1);
        Pattern pattern2 = Pattern.compile(regex2);
        Pattern pattern3 = Pattern.compile(regex3);
        Pattern pattern4 = Pattern.compile(regex4);
        Pattern pattern5 = Pattern.compile(regex5);
        Pattern pattern6 = Pattern.compile(regex6);
        Pattern pattern7 = Pattern.compile(regex7);
        Pattern pattern8 = Pattern.compile(regex8);
        Matcher matcher1 = pattern1.matcher(texto);
        Matcher matcher2 = pattern2.matcher(texto);
        Matcher matcher3 = pattern3.matcher(texto);
        Matcher matcher4 = pattern4.matcher(texto);
        Matcher matcher5 = pattern5.matcher(texto);
        Matcher matcher6 = pattern6.matcher(texto);
        Matcher matcher7 = pattern7.matcher(texto1);
        Matcher matcher8 = pattern8.matcher(texto2);

        System.out.println("texto:  " + texto);
        System.out.println("regex:  " + regex1);
        while (matcher1.find()) {
            System.out.print(matcher1.start() + " " + matcher1.group() + " ");
        }

        System.out.println("\n----------------------------");
        System.out.println("texto:  " + texto);
        System.out.println("regex:  " + regex2);
        while (matcher2.find()) {
            System.out.print(matcher2.start() + " " + matcher2.group() + " ");
        }

        System.out.println("\n----------------------------");
        System.out.println("texto:  " + texto);
        System.out.println("regex:  " + regex3);
        while (matcher3.find()) {
            System.out.print(matcher3.start() + " " + matcher3.group() + " ");
        }

        System.out.println("\n----------------------------");
        System.out.println("texto:  " + texto);
        System.out.println("regex:  " + regex4);
        while (matcher4.find()) {
            System.out.print(matcher4.start() + " " + matcher4.group() + " ");
        }

        System.out.println("\n----------------------------");
        System.out.println("texto:  " + texto);
        System.out.println("regex:  " + regex5);
        while (matcher5.find()) {
            System.out.print(matcher5.start() + " " + matcher5.group() + " ");
        }

        System.out.println("\n----------------------------");
        System.out.println("texto:  " + texto);
        System.out.println("regex:  " + regex6);
        while (matcher6.find()) {
            System.out.print(matcher6.start() + " " + matcher6.group() + " ");
        }

        System.out.println("\n----------------------------");
        System.out.println("texto:  " + texto1);
        System.out.println("regex:  " + regex7);
        while (matcher7.find()) {
            System.out.print(matcher7.start() + " " + matcher7.group() + "\n");
        }

        System.out.println("\n----------------------------");
        System.out.println("texto:  " + texto2);
        System.out.println("regex:  " + regex8);
        while (matcher8.find()) {
            System.out.print(matcher8.start() + " " + matcher8.group() + "\n");
        }
    }
}
