package academy.devdojo.maratonajava.javacore.Uregex.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcherTest01 {
    public static void main(String[] args) {
        String regex = "aba";
        String texto = "abaaba";
        String texto2 = "abababa";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(texto);
        Matcher matcher1 = pattern.matcher(texto2);

        System.out.println("texto:  "+texto);
        System.out.println("indice: 0123456789");
        System.out.println("regex:  "+regex);
        while (matcher.find()){
            System.out.println(matcher.start()+" ");
        }

        System.out.println("-----------------------");
        System.out.println("texto:  "+texto2);
        System.out.println("indice: 0123456789");
        System.out.println("regex:  "+regex);
        while (matcher1.find()){
            System.out.println(matcher1.start()+" ");
        }
    }
}
