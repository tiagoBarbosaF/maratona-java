package academy.devdojo.maratonajava.javacore.Dconstrutores.test;


import academy.devdojo.maratonajava.javacore.Dconstrutores.dominio.Anime;

public class AnimeTest01 {
    public static void main(String[] args) {
//        anime.init("Castlevania","TV", 12);
//        anime.init("Castlevania","TV", 12,"Ação");
//        anime.setNome("Castlevania");
//        anime.setTipo("TV");
//        anime.setEpisodios(12);
        Anime anime = new Anime("Haikyuu","TV",68,"Sport","Production IG");
        Anime anime2 = new Anime();

        anime.imprime();
        anime2.imprime();
    }
}
