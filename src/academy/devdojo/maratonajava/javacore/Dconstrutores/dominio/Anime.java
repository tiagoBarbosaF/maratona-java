package academy.devdojo.maratonajava.javacore.Dconstrutores.dominio;

public class Anime {
    private String nome;
    private String tipo;
    private int episodios;
    private String genero;
    private String estudio;

    public Anime(String nome, String tipo, int episodios, String genero){
        this.nome = nome;
        this.tipo = tipo;
        this.episodios = episodios;
        this.genero = genero;
    }

    public Anime(String nome, String tipo, int episodios, String genero, String estudio){
        this(nome,tipo,episodios,genero);
        this.estudio = estudio;
    }

    // Sobrecarga de construtores
    public Anime(){

    }

//    public void init(String nome, String tipo, int episodios) {
//        this.nome = nome;
//        this.tipo = tipo;
//        this.episodios = episodios;
//    }
//
//    // Sobrecarga de métodos consiste em criar um novo método com o mesmo nome, acrescentando mais parâmetros
//    public void init(String nome, String tipo, int episodios, String genero) {
//        this.init(nome, tipo, episodios);
//        this.genero = genero;
//    }

    public void imprime() {
        System.out.println("Nome: " + this.nome + " | Tipo: " + this.tipo + " | Episódios: " + this.episodios +
                " | Gênero: " + this.genero+" | Estúdio: "+this.estudio);
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setEpisodios(int episodios) {
        this.episodios = episodios;
    }

    public int getEpisodios() {
        return this.episodios;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
