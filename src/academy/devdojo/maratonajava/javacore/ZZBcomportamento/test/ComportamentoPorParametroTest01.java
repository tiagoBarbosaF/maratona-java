package academy.devdojo.maratonajava.javacore.ZZBcomportamento.test;

import academy.devdojo.maratonajava.javacore.ZZBcomportamento.dominio.Car;

import java.util.ArrayList;
import java.util.List;

public class ComportamentoPorParametroTest01 {
    private static List<Car> cars = List.of(new Car("green", 2011), new Car("black", 1998), new Car("red", 2016));

    public static void main(String[] args) {
        System.out.println(filterCarByColor(cars,"green"));
        System.out.println(filterCarByColor(cars,"red"));
        System.out.println(filterAge(cars,2015));
    }

    private static List<Car> filterCarByColor(List<Car> cars, String cor){
        List<Car> filteredCar = new ArrayList<>();
        for (Car car : cars) {
            if (car.getColor().equals(cor)){
                filteredCar.add(car);
            }
        }
        Car car;
        return filteredCar;
    }

    private static List<Car> filterAge(List<Car> cars, int year){
        List<Car> oldCars = new ArrayList<>();
        for (Car car : cars) {
            if (car.getYear() < year){
                oldCars.add(car);
            }
        }
        return oldCars;
    }
}
