package academy.devdojo.maratonajava.javacore.Rdatas.test;

import java.time.*;
import java.time.chrono.JapaneseDate;
import java.util.Map;

public class ZoneTest01 {
    public static void main(String[] args) {
        Map<String, String> shortIds = ZoneId.SHORT_IDS;
        System.out.println(shortIds);
        System.out.println(ZoneId.systemDefault());

        ZoneId tokyoZone = ZoneId.of("Asia/Tokyo");
        System.out.println("---------------");
        System.out.println(tokyoZone);

        LocalDateTime now = LocalDateTime.now();
        System.out.println("---------------");
        System.out.println(now);

        ZonedDateTime zonedDateTime = now.atZone(tokyoZone);
        System.out.println("-----------------");
        System.out.println(zonedDateTime);

        Instant nowInstant = Instant.now();
        System.out.println("----------------");
        System.out.println(nowInstant);
        ZonedDateTime zonedDateTime1 = nowInstant.atZone(tokyoZone);
        System.out.println("----------------");
        System.out.println(zonedDateTime1);

        System.out.println("------------------");
        System.out.println(ZoneOffset.MIN);
        System.out.println(ZoneOffset.MAX);

        ZoneOffset offsetManaus = ZoneOffset.of("-04:00");
        OffsetDateTime ZoneManaus = now.atOffset(offsetManaus);
        System.out.println("------------------");
        System.out.println(ZoneManaus);
        OffsetDateTime ZoneManaus1 = OffsetDateTime.of(now, offsetManaus);
        System.out.println("----------------");
        System.out.println(ZoneManaus1);

        OffsetDateTime offsetDateTime = nowInstant.atOffset(offsetManaus);
        System.out.println("---------------");
        System.out.println("Tempo " + ZoneId.systemDefault() + " " + now);
        System.out.println("Tempo Manaus " + offsetDateTime);

        JapaneseDate japaneseDate = JapaneseDate.from(LocalDate.now());
        System.out.println("----------------");
        System.out.println(japaneseDate);

        LocalDate japanEra = LocalDate.of(1900, 2, 1);
        JapaneseDate japanEraTime = JapaneseDate.from(japanEra);
        System.out.println("---------------");
        System.out.println(japanEraTime);

    }
}
