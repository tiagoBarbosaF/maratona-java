package academy.devdojo.maratonajava.javacore.ZZClambdas.test;


import academy.devdojo.maratonajava.javacore.ZZClambdas.dominio.Anime;
import academy.devdojo.maratonajava.javacore.ZZClambdas.service.AnimeComparator;

import java.util.ArrayList;
import java.util.List;

public class MethodReferenceTest01 {
    public static void main(String[] args) {
        List<Anime> animeList = new ArrayList<>(List.of(new Anime("Berserk",43), new Anime("One Piece",900), new Anime("Naruto", 500)));
        List<Anime> animeList2 = new ArrayList<>(List.of(new Anime("Berserk",43), new Anime("One Piece",100), new Anime("Naruto", 500)));
//        Collections.sort(animeList, (a1,a2) -> a1.getTitle().compareTo(a2.getTitle()));
        animeList.sort(AnimeComparator::compareByTitle);
        System.out.println(animeList);
        System.out.println("---------------");
        animeList2.sort(AnimeComparator::compareByEpisodes);
        System.out.println(animeList2);
    }
}
